# Ido dictionary formatter

## Plaintext dictionary formats

This is what makes converting the files tricky... have to write different
logic to parse each one.

### de-to-ido.txt

```
A


-a (gram.)  (Eigenschaftswort)
a (muz.)  Note, Ton (absol.)
a (= ad) {prep.} (prep.)  nach, zu, an (Dativ)
abad.o (rel.)  Abt
abad.ey.o  Abtei
abak.o  Abakus, Rechenbrett; Deckplatte einer Säule
```

Will have to skip header letters. First word is Ido, and everything after
can be parsed as "de".

### en-to-ido.txt

```
A

a: (the indefinite article is not ordinarily translated, if a certain but only partially defined person or thing is to be indicated ula may be used).

A: (mus.) Ab: aes; A# ais.

aback: dope; to be taken a.: esar subite surpriz-ita o konfuz-ita o konstern-ita.

abacus: abako.

abaft: (prep.) dop; (adv.) ad-dope.
```

Have to skip header letters. Can split Ido from English using the first colon.

### eo-to-ido.txt

```
A
abako abako

abdiki abdikar (t)

abdomeno abdomino

abelo abelo

abio abieto
```

Skip header letters

Just split first word from all other words

### es-to-ido.txt

```
A
a, a, ad.
abad, abado.
abajo, (debajo de) sub; (en la parte inferior) infre.
abanderado, standardiero.
abandon-ar, -ado, abandon-ar, -ita.
abanico, -ar, abaniko, ventizar.
abaratar, chipigar.
abarcar, embracar.
```

Split on first comma

### ido-to-de.txt

```
 A

a (gram.) - (Eigenschaftswort)
a (= ad) (prep.) - nach, zu, an (Dativ)
abad.o - Abt
abak.o - Abakus, Rechenbrett; Deckplatte einer Säule
```

Split on -


### ido-to-en.txt

```
A

a (ad): (general sense) to. indicating that toward which there is movment, tendency, or position, with or without arrival; opposed to from (de, ek); (cf. vers, til); il iris a la kirko: he went to (the) church; il venas de Paris a London: he is on his way from Paris to London; la hundo jetis su a la kato: the dog sprang at the cat; del (de la) esto al (ad la) westo: from (the) east to (the) west; de tempo a tempo: from time to time; de un dio a l'altra: from one day to another, from day to day; de la supro a l'infro: from top to bottom; (dative: indirect object) donez a me la bastono: give me the stick; il parolis ad el: he spoke to her. (object of action, thought, desire) il elevas su a la richeso e a la honori: he is rising to weather and honors; atencema a la diskurso, attentive to the discourse; surda a la ditreso-krii: deaf to the cries of distress; amo a Deo: love to God; me deziras a vu omna feliceso: I wish you all happiness. (comparison or relation) agreabla a la gusto: agreeable to the taste; ca okupo konvenas ad il: this occupation suits him; (proportion; total) tri raportas a non quale du a sis: three is to nine as two is to six; evaluar lua revenuo a 10.000 franki; taxar ol a 400 franki: to estimate his income as (amounting to) 10,000 francs; to tax it at (at the rate of, a total of) 400 francs. Note: Ad is often joined to other prepositions, to verbal roots, adverbs, particularly to add the idea of motion. — FILS

abad-(ul)o: abbot; -ino: abbess; -eyo: abbey; -eso: abbotship; -ala, -ey-ala: abbatial. — DeFIRS

abak-o*: (math., arch.) abacus. — DEFIS
```

Split on first colon


### ido-to-eo.txt

```
A

a(d) al

abako abako

abandonar (t) forlasi, forjxeti

abasar (t) malaltigi
```

Split on first word / the rest


### ido-to-es.txt

```
A
A, a (absoluto); la (mús.).
a, (prep. ante consonante); ad (ante vocal), a, hacia.
-a, (final del adjetivo).
-ab-, (sufijo de los tiempos secundarios en los verbos).
abad-o, abad.
abak-o, ábaco.
abandonar, abandonar.
abaniko, abanico.
```

Split on first comma


### ido-to-fi.txt

```
A

a/ad : -lle, luokse

abad.o : apotti, abbedissa

abandon.ar : hylätä

abat.ar : kopauttaa

abel.o : mehiläinen

abiet.o : (jalo)kuusi
```

Split on colon


### ido-to-fr.txt

```A

a (= ad) (prép.) à, vers
-a, finale de l'adjectif
-ab- ( suffixe verbal: suffixe des temps secondaires)
abad.o abbé
abako abaque, tailloir
abandon.ar (transitif) abandonner, laisser
abanik.o éventail
abas.ar (transitif) abaisser (sens propre, faire descendre à un niveau plus bas)
abat.ar (transitif) abattre (sens propre)
```

Split on first word / everything else


### ido-to-interlingua.txt

```A

a: a

abandonar: abandonar

abatar: abatter

abonar: abonar se a

abrupta: abrupte
```

Split on colon


### ido-to-it.txt

```
A

a (= ad) a, ad, verso [dativo, tendenza, mira, direzione]

-ab- [suffisso dei tempi anteriori]

abad.o abate

abad.ey.o abbazia

abak.o abaco

abandon.ar [t] abbandonare

abanik.o ventaglio
```

Split on first word / everything else


### ido-to-jp.txt

```
A

a(d) [prep] …に向かって, …へ, …の所へ, …に対して, …に

abad/o 僧院長，修道院長

abak/o 計算板, そろばん

abandon/ar t. 見捨てる,（地位を）投げ打つ, 諦める, 断念する

abanik/o 扇

abas/ar t. 下げる, 低くする, 降ろす; f. …の品位（地位）を落とす
```


### ido-to-nl.txt

```
A

a/ad : aan, naar

abonar : abonneren op

acensar : omhooggaan, (be)stijgen

adparolar : aanspreken
```

Split on colon


### ido-to-pt.txt

```
A

-ano(suffixo) sócio, partidario

A, ad a, para

Abato abbade

Abelo abelha

Abonar asignar (jornal)

Aboyar latir, ganir
```

Split on first word



### ido-to-ru.txt

```
A
A(D) К (движение к)
ABADINO аббатисса
ABADO Аббат, аббатисса (ср. род)
ABADULO аббат
ABANDONAR Отказываться
ABASAR Понизить, понижать качество (нравственно)
ABATAR поразить, сражать
```

Split on first word


### jp-to-ido.txt


```
 A

aakutoo【アーク灯】: arklampo

aati【アーチ】: arko

aa【ああ】: ha! ho ve!

abaku【暴く】: expozar

abara【肋】: kosto

abareru【暴れる】: agar furioze, violentar (t)
```

Split on colon



## Dictionary Credits

* English - Ido from http://www.idolinguo.org.uk/eniddyer.htm
* Ido - English from http://www.idolinguo.org.uk/idendyer.htm
* Español - Ido from http://kanaria1973.ido.li/krayono/dicespido.html
* Ido - Español from http://kanaria1973.ido.li/krayono/dicidoesp.html
* 日本語 - Ido from http://kanaria1973.ido.li/dicjaponaido.html
* Ido - 日本語 from http://kanaria1973.ido.li/dicidojapona.html
* Deutsch - Ido from http://idolinguo.org.uk/idger.htm
* Ido - Deutsch from http://kanaria1973.ido.li/dicidogermana.html
* Ido - Français from http://kanaria1973.ido.li/dicidofranca.html
* Ido - русский from http://kanaria1973.ido.li/krayono/dicidorusa.html
* Ido - Italiano from http://kanaria1973.ido.li/dicidoitaliano.html
* Ido - Portugues from http://kanaria1973.ido.li/dicidoportugues.html
* Ido - Nederlands from http://kanaria1973.ido.li/dicidonederlandana.html
* Ido - Suomi from http://kanaria1973.ido.li/dicidofinlandana.html
* Ido - Esperanto from http://kanaria1973.ido.li/dicidoesperanto.html
* Esperanto - Ido (kanaria, but from wayback machine)
* Ido - Interlingua (kanaria, but from wayback machine)
