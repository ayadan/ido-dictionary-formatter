textFile = open( "plaintext/ido-to-en.txt", "r" )
csvFile = open( "csv/ido-to-en.csv", "w" )

# Format:
# -i : to (infinitive ending)

csvFile.write( "ido,english\n" )

for count, line in enumerate( textFile ):
    line = line.replace( "\"", "\\\"" )
    line = line.strip( "\r\n" )
    
    cols = line.split( ":", 1 )
    if ( len( cols ) < 2 ):
        continue    # skip blank lines and single letters
    
    ido = cols[0].strip( " " )
    english = cols[1].strip( " " )
    
    print( english, ido )
    csvFile.write( "\"" + ido + "\",\"" + english + "\"\n" )
